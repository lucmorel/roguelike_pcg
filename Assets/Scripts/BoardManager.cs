﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;


public class BoardManager : MonoBehaviour
{
    // Using Serializable allows us to embed a class with sub properties in the inspector.
    [Serializable]
    public class Count
    {
        // Minimum value for our Count class.
        public int minimum;
        // Maximum value for our Count class.
        public int maximum;

        // Assignment constructor.
        public Count(int min, int max)
        {
            minimum = min;
            maximum = max;
        }
    }

    public int columns = 5;
    public int rows = 5;
    public GameObject enemy;
    public GameObject exit;
    public GameObject chestTile;
    public GameObject[] floorTiles;
    public GameObject[] wallTiles;
    public GameObject[] outerWallTiles;

    private Transform boardHolder;
    private Transform dungeonBoardHolder;
    private Dictionary<Vector2, Vector2> gridPositions = new Dictionary<Vector2, Vector2>();
    private Dictionary<Vector2, Vector2> dungeonGridPositions;

    public void BoardSetup()
    {
        boardHolder = new GameObject("Board").transform;

        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                // We add the tiles positions to our dictionnary
                gridPositions.Add(new Vector2(x, y), new Vector2(x, y));

                // We choose a random tile in our floorTiles array
                GameObject tile = floorTiles[Random.Range(0, floorTiles.Length)];
                // We instantiate it at its position
                GameObject instance = Instantiate(tile, new Vector3(x, y, 0f), Quaternion.identity);

                // Then we set its parent as the board holder to keep it organized
                instance.transform.SetParent(boardHolder);
            }
        }
    }

    public void AddToBoard(int horizontal, int vertical)
    {
        int x = (int)Player.position.x;
        int y = (int)Player.position.y;
        int sightX = 0, sightY = 0;
        
        // Moving right
        if (horizontal == 1)
        {
            sightX = (x + 2);
            for (x += 1; x <= sightX; x++)
            {
                y = (int)Player.position.y;
                sightY = (y + 1);
                for (y -= 1; y <= sightY; y++)
                {
                    AddTile(new Vector2(x, y));
                }
            }
        }

        // Moving left
        else if (horizontal == -1)
        {
            sightX = (x - 2);
            for (x -= 1; x >= sightX; x--)
            {
                y = (int)Player.position.y;
                sightY = (y + 1);
                for (y -= 1; y <= sightY; y++)
                {
                    AddTile(new Vector2(x, y));
                }
            }
        }

        // Moving up
        else if (vertical == 1)
        {
            sightY = (y + 2);
            for (y += 1; y <= sightY; y++)
            {
                x = (int)Player.position.x;
                sightX = (x + 1);
                for (x -= 1; x <= sightX; x++)
                {
                    AddTile(new Vector2(x, y));
                }
            }
        }

        // Moving down
        else if (vertical == -1)
        {
            sightY = (y - 2);
            for (y -= 1; y >= sightY; y--)
            {
                x = (int)Player.position.x;
                sightX = (x + 1);
                for (x -= 1; x <= sightX; x++)
                {
                    AddTile(new Vector2(x, y));
                }
            }
        }
    }

    private void AddTile(Vector2 tilePos)
    {
        if (!gridPositions.ContainsKey(tilePos))
        {
            gridPositions.Add(tilePos, tilePos);

            GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];
            GameObject instance = Instantiate(toInstantiate, tilePos, Quaternion.identity);

            instance.transform.SetParent(boardHolder);

            // Put a wall above
            if (Random.Range(0, 3) == 1)
            {
                toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
                instance = Instantiate(toInstantiate, tilePos, Quaternion.identity);

                instance.transform.SetParent(boardHolder);
            }

            // Put a dungeon entrance above
            else if (Random.Range(0, 100) == 1)
            {
                toInstantiate = exit;
                instance = Instantiate(toInstantiate, tilePos, Quaternion.identity);

                instance.transform.SetParent(boardHolder);
            }

            // Instantiate an enemy
            else if (Random.Range(0, 20) == 1)
            {
                toInstantiate = enemy;
                instance = Instantiate(toInstantiate, tilePos, Quaternion.identity);

                instance.transform.SetParent(boardHolder);
            }
        }
    }

    public void SetDungeonBoard(Dictionary<Vector2, TileType> dungeonTiles, int bound, Vector2 endPos)
    {
        boardHolder.gameObject.SetActive(false);
        dungeonBoardHolder = new GameObject("Dungeon").transform;

        GameObject tileToInstantiate, instance;

        foreach (KeyValuePair<Vector2, TileType> tile in dungeonTiles)
        {
            tileToInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];
            instance = Instantiate(tileToInstantiate, tile.Key, Quaternion.identity);
            instance.transform.SetParent(dungeonBoardHolder);

            if (tile.Value == TileType.Chest)
            {
                instance = Instantiate(chestTile, tile.Key, Quaternion.identity);
                instance.transform.parent = dungeonBoardHolder;
            }
            else if (tile.Value == TileType.Enemy)
            {
                instance = Instantiate(enemy, tile.Key, Quaternion.identity);
                instance.transform.parent = dungeonBoardHolder;
            }
        }

        for (int x = -1; x <= bound; x++)
        {
            for (int y = (DungeonManager.minY - 1); y <= (DungeonManager.maxY + 1); y++)
            {
                if (!dungeonTiles.ContainsKey(new Vector2(x, y)))
                {
                    tileToInstantiate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];
                    instance = Instantiate(tileToInstantiate, new Vector2(x, y), Quaternion.identity);

                    instance.transform.SetParent(dungeonBoardHolder);
                }
            }
        }

        tileToInstantiate = exit;
        instance = Instantiate(tileToInstantiate, endPos, Quaternion.identity);

        instance.transform.SetParent(dungeonBoardHolder);
    }

    public void SetWorldBoard()
    {
        Destroy(dungeonBoardHolder.gameObject);
        boardHolder.gameObject.SetActive(true);
    }

    public bool CheckValidTile(Vector2 pos)
    {
        return gridPositions.ContainsKey(pos);
    }
}