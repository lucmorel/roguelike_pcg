﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(SpriteRenderer))]
public class Item : MonoBehaviour {
    
    public enum ItemType
    {
        Gloves,
        Boots
    }

    public enum ItemLevel
    {
        Common,
        Magic,
        Rare,
        Epic,
        Legendary
    }

    [Header("Item sprites")]
    public Sprite glovesSprite;
    public Sprite bootsSprite;

    [Header("Item configuration")]
    public ItemType type;
    public ItemLevel level;
    public Color levelColor;
    public int attackMod, defenseMod;

    private SpriteRenderer spriteRenderer;

    public void RandomItemInit()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        SelectItem();
    }

    private void SelectItem()
    {
        int itemCount = Enum.GetValues(typeof(ItemType)).Length;
        type = (ItemType)Random.Range(0, itemCount);

        switch(type)
        {
            case ItemType.Boots:
                attackMod = 0;
                defenseMod = Random.Range(1, 4);
                spriteRenderer.sprite = bootsSprite;
                break;
            case ItemType.Gloves:
                attackMod = Random.Range(1, 4);
                defenseMod = 0;
                spriteRenderer.sprite = glovesSprite;
                break;
        }

        int randomLevel = Random.Range(0, 101);
        if (randomLevel >= 25 && randomLevel < 50)
        {
            level = ItemLevel.Magic;
            spriteRenderer.color = levelColor = Color.green;
            attackMod += Random.Range(1, 4);
            defenseMod += Random.Range(1, 4);
        }
        else if (randomLevel >= 50 && randomLevel < 75)
        {
            level = ItemLevel.Rare;
            spriteRenderer.color = levelColor = Color.yellow;
            attackMod += Random.Range(4, 10);
            defenseMod += Random.Range(4, 10);
        }
        else if (randomLevel >= 75 && randomLevel < 90)
        {
            level = ItemLevel.Epic;
            spriteRenderer.color = levelColor = Color.blue;
            attackMod += Random.Range(10, 18);
            defenseMod += Random.Range(10, 18);
        }
        else if (randomLevel >= 90)
        {
            level = ItemLevel.Legendary;
            spriteRenderer.color = levelColor = Color.magenta;
            attackMod += Random.Range(18, 26);
            defenseMod += Random.Range(18, 26);
        } else
        {
            level = ItemLevel.Common;
            spriteRenderer.color = levelColor = Color.white;
        }

    }
}
