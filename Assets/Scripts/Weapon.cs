﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public bool inPlayerInventory = false;

    [Header("Scripting animation values")]
    public float degreeY = 0, degreeZ = -90f;
    public float animationSpeed = 20f;

    private Player player;
    private WeaponComponent[] weaponComps;
    private bool weaponUsed = false;
    private bool invertAnimation = false;

    private void Update()
    {
        if (inPlayerInventory)
        {
            Vector3 euler = new Vector3(0, degreeY, degreeZ);
            invertAnimation = (transform.parent.localScale.x < 0) ? true : false;
            if (invertAnimation)
                euler.z = -degreeZ;
            else
                euler.z = degreeZ;
        
            transform.position = player.transform.position;
            if (weaponUsed)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(euler), Time.deltaTime * animationSpeed);
                if (Mathf.Abs(Mathf.Abs(euler.z) - (transform.eulerAngles.z % 180)) <= 0.1f)
                {
                    transform.localEulerAngles = Vector3.zero;
                    weaponUsed = false;
                    EnableSpriteRender(false);
                }
            }
        }
    }

    public void AcquireWeapon()
    {
        player = GetComponentInParent<Player>();
        weaponComps = GetComponentsInChildren<WeaponComponent>();
    }

    public void UseWeapon()
    {
        EnableSpriteRender(true);
        weaponUsed = true;
    }

    public void EnableSpriteRender(bool enabled)
    {
        foreach (WeaponComponent component in weaponComps)
            component.GetSpriteRenderer().enabled = enabled;
    }

    public Sprite GetComponentImage(int index)
    {
        return weaponComps[index].GetSpriteRenderer().sprite;
    }
}
