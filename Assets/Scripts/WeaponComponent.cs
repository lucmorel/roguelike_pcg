﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class WeaponComponent : MonoBehaviour
{
	public Sprite[] modules;

	private Weapon parent;
	private SpriteRenderer spriteRenderer;

	void Start()
	{
		parent = GetComponentInParent<Weapon>();
		spriteRenderer = GetComponent<SpriteRenderer>();
		spriteRenderer.sprite = modules[Random.Range(0, modules.Length)];
	}

	void Update()
	{
		transform.eulerAngles = parent.transform.eulerAngles;
	}

	public SpriteRenderer GetSpriteRenderer()
	{
		return spriteRenderer;
	}
}

