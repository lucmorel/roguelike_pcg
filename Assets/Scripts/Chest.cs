﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(SpriteRenderer))]
public class Chest : MonoBehaviour
{

    public Sprite openChestSprite;
    public Item randomItem;
    public Weapon weapon;

    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Open()
    {
        spriteRenderer.sprite = openChestSprite;

        GameObject toInstantiate;

        if (Random.Range(0, 2) == 1)
        {
            randomItem.RandomItemInit();
            toInstantiate = randomItem.gameObject;
        }
        else
        {
            toInstantiate = weapon.gameObject;
        }

        GameObject instance = Instantiate(toInstantiate, transform.position, Quaternion.identity);
        instance.transform.parent = transform.parent;

        gameObject.layer = 10;
        spriteRenderer.sortingLayerName = "Items";
    }
}
