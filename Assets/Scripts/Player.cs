﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Player inherits from MovingObject, our base class for objects that can move, Enemy also inherits from this.
public class Player : MovingObject
{
    public Image boots;
    public Image gloves;
    public Image weaponComp1, weaponComp2, weaponComp3;
    public GameObject weaponUI;

    public int attackMod = 0;
    public int defenseMod = 0;

    public int wallDamage = 1;
    public Text healthText;
    public static Vector2 position;

    [HideInInspector] public bool onWorldBoard;
    [HideInInspector] public bool dungeonTransition;

    // Used to store a reference to the Player's animator component.
    private Animator animator;
    // Used to store player health points total during level.
    private int health;
    private int maxHealth = 100;
    private Dictionary<string, Item> inventory;
    private Weapon weapon;
    

    // Start overrides the Start function of MovingObject
    protected override void Start()
    {
        // Get a component reference to the Player's animator component
        animator = GetComponent<Animator>();

        // Get the current health point total stored in GameManager.instance between levels.
        health = GameManager.instance.healthPoints;

        // Set the healthText to reflect the current player health total.
        UpdateHealthText();

        position = new Vector2(2, 2);

        onWorldBoard = true;
        dungeonTransition = false;

        inventory = new Dictionary<string, Item>();

        weaponComp1.preserveAspect = true;
        weaponComp2.preserveAspect = true;
        weaponComp3.preserveAspect = true;

        // Call the Start function of the MovingObject base class.
        base.Start();
    }

    private void Update()
    {
        // If it's not the player's turn, exit the function.
        if (!GameManager.instance.playersTurn)
            return;

        bool canMove = false;

        // Used to store the horizontal move direction.
        int horizontal = 0;
        // Used to store the vertical move direction.
        int vertical = 0;

        // Get input from the input manager, round it to an integer and store in horizontal to set x axis move direction
        horizontal = (int)(Input.GetAxisRaw("Horizontal"));

        Vector3 scale = transform.localScale;
        if (horizontal < 0)
            scale.x = -1;
        else if (horizontal > 0)
            scale.x = 1;
        transform.localScale = scale;

        // Get input from the input manager, round it to an integer and store in vertical to set y axis move direction
        vertical = (int)(Input.GetAxisRaw("Vertical"));

        // Check if moving horizontally, if so set vertical to zero.
        if (horizontal != 0)
        {
            vertical = 0;
        }

        // Check if we have a non-zero value for horizontal or vertical
        if (horizontal != 0 || vertical != 0)
        {
            // Call AttemptMove passing in the generic parameter Wall, since that is what Player may interact with if they encounter one (by attacking it)
            // Pass in horizontal and vertical as parameters to specify the direction to move Player in.
            if (!dungeonTransition)
            {
                if (onWorldBoard)
                {
                    canMove = AttemptMove<Wall>(horizontal, vertical);
                }
                else
                {
                    canMove = AttemptMove<Chest>(horizontal, vertical);
                }
                if (canMove && onWorldBoard)
                {
                    position.x += horizontal;
                    position.y += vertical;
                    GameManager.instance.UpdateBoard(horizontal, vertical);
                }
            }
        }
    }

    // AttemptMove overrides the AttemptMove function in the base class MovingObject
    // AttemptMove takes a generic parameter T which for Player will be of the type Wall, it also takes integers for x and y direction to move in.
    protected override bool AttemptMove<T>(int xDir, int yDir)
    {
        // Call the AttemptMove method of the base class, passing in the component T (in this case Wall) and x and y direction to move.
        bool hit = base.AttemptMove<T>(xDir, yDir);

        // Set the playersTurn boolean of GameManager to false now that players turn is over.
        GameManager.instance.playersTurn = false;

        return hit;
    }


    // OnCantMove overrides the abstract function OnCantMove in MovingObject.
    // It takes a generic parameter T which in the case of Player is a Wall which the player can attack and destroy.
    protected override void OnCantMove<T>(T component)
    {
        if (typeof(T) == typeof(Wall))
        {
            Wall hitWall = component as Wall;
            hitWall.DamageWall(wallDamage);
        }
        else if (typeof(T) == typeof(Chest))
        {
            Chest chest = component as Chest;
            chest.Open();
        }

        animator.SetTrigger("playerChop");
        if (weapon)
            weapon.UseWeapon();
    }

    // LoseHealth is called when an enemy attacks the player.
    // It takes a parameter loss which specifies how many points to lose.
    public void LoseHealth(int loss)
    {
        // Set the trigger for the player animator to transition to the playerHit animation.
        animator.SetTrigger("playerHit");

        // Subtract lost health points from the players total.
        health -= loss;

        // Update the health display with the new total.
        healthText.text = "-" + loss + " Health: " + health;

        // Check to see if game has ended.
        CheckIfGameOver();
    }


    // CheckIfGameOver checks if the player is out of health points and if so, ends the game.
    private void CheckIfGameOver()
    {
        // Check if health point total is less than or equal to zero.
        if (health <= 0)
        {
            // Call the GameOver function of GameManager.
            GameManager.instance.GameOver();
        }
    }

    private void GoDungeonPortal()
    {
        if (onWorldBoard)
        {
            onWorldBoard = false;
            GameManager.instance.EnterDungeon();
            transform.position = DungeonManager.startPos;
        }
        else
        {
            onWorldBoard = true;
            GameManager.instance.ExitDungeon();
            transform.position = position;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Interaction with dungeon entrance/exit
        if (other.tag == "Exit")
        {
            dungeonTransition = true;
            Invoke("GoDungeonPortal", 0.5f);
            Destroy(other.gameObject);
        }

        // Interaction with food and soda (health items)
        else if (other.tag == "Food" || other.tag == "Soda")
        {
            UpdateHealth(other);
        }

        // Interaction with chest's items
        else if (other.tag == "Item")
        {
            UpdateInventory(other);
        }

        // Interaction with weapons
        else if (other.tag == "Weapon")
        {
            if (weapon)
                Destroy(transform.GetChild(0).gameObject); 

            other.enabled = false;
            other.transform.parent = transform;
            weapon = other.GetComponent<Weapon>();
            weapon.AcquireWeapon();
            weapon.inPlayerInventory = true;
            weapon.EnableSpriteRender(false);
            wallDamage = attackMod + 3;
            weaponComp1.sprite = weapon.GetComponentImage(0);
            weaponComp2.sprite = weapon.GetComponentImage(1);
            weaponComp3.sprite = weapon.GetComponentImage(2);
            weaponUI.SetActive(true);
        }
    }

    private void UpdateHealth(Collider2D healthItem)
    {
        if (health < maxHealth)
        {
            if (healthItem.tag == "Food")
                health += Random.Range(1, 6);
            else
                health += Random.Range(6, 11);

            health = Mathf.Clamp(health, 0, maxHealth);
            Destroy(healthItem.gameObject);
        }
        GameManager.instance.healthPoints = health;
        UpdateHealthText();
    }

    private void UpdateHealthText()
    {
        healthText.text = "Health: " + health;
    }

    private void UpdateInventory(Collider2D itemCollider)
    {
        Item item = itemCollider.GetComponent<Item>();
        switch (item.type)
        {
            case Item.ItemType.Gloves:
                // There are no gloves equiped yet
                if (!inventory.ContainsKey("gloves"))
                {
                    inventory.Add("gloves", item);
                    gloves.gameObject.SetActive(true);
                }
                // There are gloves equiped, but at higher level than the new item, just return
                else if (inventory["gloves"].level >= item.level)
                    return;
                // There are gloves equiped, and the new ones are better, replace it
                else
                    inventory["gloves"] = item;
                
                // The spawned item is destroyed and the icon change only when equiped change, thanks to the return
                Destroy(itemCollider.gameObject);
                gloves.color = item.levelColor;
                break;
            case Item.ItemType.Boots:
                // Same reasoning than above
                if (!inventory.ContainsKey("boots"))
                {
                    inventory.Add("boots", item);
                    boots.gameObject.SetActive(true);
                    Destroy(itemCollider.gameObject);
                }
                else if (inventory["boots"].level >= item.level)
                    return;
                else
                    inventory["boots"] = item;

                Destroy(itemCollider.gameObject);
                boots.color = item.levelColor;
                break;
        }

        attackMod = defenseMod = 0;

        foreach (KeyValuePair<string, Item> gear in inventory)
        {
            attackMod += gear.Value.attackMod;
            defenseMod += gear.Value.defenseMod;
        }

        if (weapon)
            wallDamage = attackMod + 3;
    }
}

