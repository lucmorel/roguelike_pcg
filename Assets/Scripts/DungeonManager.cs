﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum TileType
{
    Essential,
    Random,
    Chest,
    Enemy,
    Empty
}

public class DungeonManager : MonoBehaviour
{
    [Serializable]
    public class PathTile
    {
        public TileType type;
        public Vector2 position;
        public List<Vector2> adjacentPathTiles;

        public PathTile(TileType type, Vector2 position, int min, int max, Dictionary<Vector2, TileType> currentTiles)
        {
            this.type = type;
            this.position = position;
            adjacentPathTiles = GetAdjacentPath(min, max, currentTiles);
        }

        public List<Vector2> GetAdjacentPath(int minBound, int maxBound, Dictionary<Vector2, TileType> currentTiles)
        {
            List<Vector2> pathTiles = new List<Vector2>();

            if (position.y + 1 < maxBound && !currentTiles.ContainsKey(new Vector2(position.x, position.y + 1)))
                pathTiles.Add(new Vector2(position.x, position.y + 1));

            if (position.x + 1 < maxBound && !currentTiles.ContainsKey(new Vector2(position.x + 1, position.y)))
                pathTiles.Add(new Vector2(position.x + 1, position.y));

            if (position.y - 1 > minBound && !currentTiles.ContainsKey(new Vector2(position.x, position.y - 1)))
                pathTiles.Add(new Vector2(position.x, position.y - 1));

            if (position.x - 1 >= minBound && !currentTiles.ContainsKey(new Vector2(position.x - 1, position.y)) && type != TileType.Essential)
                pathTiles.Add(new Vector2(position.x - 1, position.y));

            return pathTiles;
        }
    }

    public Dictionary<Vector2, TileType> gridPositions = new Dictionary<Vector2, TileType>();
    public int minBound = 0;
    public int maxBound;
    public static Vector2 startPos;
    public static int minY;
    public static int maxY;
    public Vector2 endPos;

    public void StartDungeon()
    {
        gridPositions.Clear();
        maxBound = Random.Range(50, 101);

        minY = maxBound;
        maxY = 0;

        BuildEssentialPath();
        BuildRandomPath();
    }

    private void BuildEssentialPath()
    {
        int randomY = Random.Range(0, maxBound + 1);
        PathTile ePath = new PathTile(TileType.Essential, new Vector2(0, randomY), minBound, maxBound, gridPositions);
        startPos = ePath.position;

        int boundTracker = 0;
        while (boundTracker < maxBound)
        {
            gridPositions.Add(ePath.position, TileType.Essential);
            UpdateMinMaxY((int)ePath.position.y);

            int adjacentTileCount = ePath.adjacentPathTiles.Count;
            int randomIndex = Random.Range(0, adjacentTileCount);
            Vector2 nextEPathPos;

            if (adjacentTileCount > 0)
                nextEPathPos = ePath.adjacentPathTiles[randomIndex];
            else
                break;

            PathTile nextEPath = new PathTile(TileType.Essential, nextEPathPos, minBound, maxBound, gridPositions);
            if (nextEPath.position.x > ePath.position.x || (nextEPath.position.x == maxBound - 1 && Random.Range(0, 2) == 1))
                ++boundTracker;

            ePath = nextEPath;
        }

        if (!gridPositions.ContainsKey(ePath.position))
        {
            gridPositions.Add(ePath.position, TileType.Essential);
            UpdateMinMaxY((int)ePath.position.y);
        }

        endPos = ePath.position;
    }

    private void BuildRandomPath()
    {
        List<PathTile> pathQueue = new List<PathTile>();
        foreach (KeyValuePair<Vector2, TileType> tile in gridPositions)
            pathQueue.Add(new PathTile(TileType.Random, tile.Key, minBound, maxBound, gridPositions));

        pathQueue.ForEach(delegate (PathTile tile)
            {
                int adjacentTileCount = tile.adjacentPathTiles.Count;
                if (adjacentTileCount > 0)
                {
                    if (Random.Range(0, 10) == 1)
                        BuildRandomChamber(tile);
                    else if (Random.Range(0, 10) == 1 || (tile.type == TileType.Random && adjacentTileCount > 1))
                    {
                        int randomIndex = Random.Range(0, adjacentTileCount);

                        Vector2 newRPathPos = tile.adjacentPathTiles[randomIndex];

                        if (!gridPositions.ContainsKey(newRPathPos))
                        {
                            if (Random.Range(0, 20) == 1)
                                gridPositions.Add(newRPathPos, TileType.Enemy);
                            else
                                gridPositions.Add(newRPathPos, TileType.Random);

                            UpdateMinMaxY((int)newRPathPos.y);

                            PathTile newRPath = new PathTile(TileType.Random, newRPathPos, minBound, maxBound, gridPositions);
                            pathQueue.Add(newRPath);
                        }
                    }
                }
            });
    }

    private void BuildRandomChamber(PathTile tile)
    {
        int chamberSize = Random.Range(3, 6);
        int adjacentTilesCount = tile.adjacentPathTiles.Count;
        int randomIndex = Random.Range(0, adjacentTilesCount);

        Vector2 chamberOrigin = tile.adjacentPathTiles[randomIndex];

        for (int x = (int)chamberOrigin.x; x < chamberOrigin.x + chamberSize; x++)
        {
            for (int y = (int)chamberOrigin.y; y < chamberOrigin.y + chamberSize; y++)
            {
                Vector2 chamberTilePos = new Vector2(x, y);
                if (!gridPositions.ContainsKey(chamberTilePos) && chamberTilePos.x < maxBound && chamberTilePos.x > minBound && chamberTilePos.y < maxBound && chamberTilePos.y > minBound)
                {
                    if (Random.Range(0, 70) == 1)
                    {
                        gridPositions.Add(chamberTilePos, TileType.Chest);
                    }
                    else
                    {
                        gridPositions.Add(chamberTilePos, TileType.Empty);
                    }
                    UpdateMinMaxY((int)chamberTilePos.y);
                }
            }
        }
    }

    private void UpdateMinMaxY(int y)
    {
        if (y < minY)
            minY = y;
        if (y > maxY)
            maxY = y;
    }
}
