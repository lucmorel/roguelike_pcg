﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    #region ATTRIBUTES

    // Delay between each Player turn.
    public float turnDelay = 0.1f;
    // Starting value for Player health points.
    public int healthPoints = 2;
    // Static instance of GameManager which allows it to be accessed by any other script.
    public static GameManager instance = null;
    // Boolean to check if it's players turn, hidden in inspector but public.
    [HideInInspector] public bool playersTurn = true;

    // Reference to our board
    private BoardManager boardScript;
    // Reference to our dungeon script
    private DungeonManager dungeonScript;
    // Reference to the player
    private Player playerScript;
    // List of all Enemy units, used to issue them move commands.
    private List<Enemy> enemies;
    // Boolean to check if enemies are moving.
    private bool enemiesMoving;
    private bool playerInDungeon;

    #endregion

    // Awake is always called before any Start functions
    void Awake()
    {
        // Check if instance already exists, if not set instance to this
        if (instance == null)
            instance = this;
        // If instance already exists and it's not this:
        else if (instance != this)
            // Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        // Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        // Assign enemies to a new List of Enemy objects.
        enemies = new List<Enemy>();

        // Get the references
        boardScript = GetComponent<BoardManager>();
        dungeonScript = GetComponent<DungeonManager>();
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        // Call the InitGame function to initialize the first level
        InitGame();
    }

    // This is called each time a scene is loaded.
    void OnLevelWasLoaded(int index)
    {
        // Call InitGame to initialize our level.
        InitGame();
    }

    // Initializes the game for each level.
    void InitGame()
    {
        // Clear any Enemy objects in our List to prepare for next level.
        enemies.Clear();

        // Call the setup of the board at beginning
        boardScript.BoardSetup();

        playerInDungeon = false;
    }

    // Update is called every frame.
    void Update()
    {
        // Check that playersTurn or enemiesMoving or doingSetup are not currently true.
        if (playersTurn || enemiesMoving)
            // If any of these are true, return and do not start MoveEnemies.
            return;

        // Start moving enemies.
        StartCoroutine(MoveEnemies());
    }

    // GameOver is called when the player reaches 0 health points
    public void GameOver()
    {
        // Disable this GameManager.
        enabled = false;
    }

    public void UpdateBoard(int horizontal, int vertical)
    {
        boardScript.AddToBoard(horizontal, vertical);
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void RemoveEnemyFromList(Enemy enemy)
    {
        enemies.Remove(enemy);
    }

    private IEnumerator MoveEnemies()
    {
        enemiesMoving = true;

        yield return new WaitForSeconds(turnDelay);

        if (enemies.Count == 0)
            yield return new WaitForSeconds(turnDelay);

        List<Enemy> enemiesToDestroy = new List<Enemy>();
        foreach (Enemy enemy in enemies)
        {
            if (playerInDungeon)
            {
                if (!enemy.GetSpriteRenderer().isVisible)
                {
                    if (enemy == enemies[enemies.Count - 1])
                        yield return new WaitForSeconds(enemy.moveTime);
                    continue;
                }
            }
            else if (!enemy.GetSpriteRenderer().isVisible || !boardScript.CheckValidTile(enemy.transform.position))
            {
                enemiesToDestroy.Add(enemy);
                continue;
            }

            enemy.MoveEnemy();

            yield return new WaitForSeconds(enemy.moveTime);
        }

        playersTurn = true;
        enemiesMoving = false;

        foreach (Enemy enemyToDestroy in enemiesToDestroy)
        {
            enemies.Remove(enemyToDestroy);
            Destroy(enemyToDestroy.gameObject);
        }

        enemiesToDestroy.Clear();
    }

    public void EnterDungeon()
    {
        dungeonScript.StartDungeon();
        boardScript.SetDungeonBoard(dungeonScript.gridPositions, dungeonScript.maxBound, dungeonScript.endPos);
        playerScript.dungeonTransition = false;
        playerInDungeon = true;

        foreach (Enemy enemy in enemies)
        {
            Destroy(enemy.gameObject);
        }

        enemies.Clear();
    }

    public void ExitDungeon()
    {
        boardScript.SetWorldBoard();
        playerScript.dungeonTransition = false;
        playerInDungeon = false;
        enemies.Clear();
    }
}