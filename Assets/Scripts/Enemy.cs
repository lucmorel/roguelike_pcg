﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator), typeof(SpriteRenderer))]
public class Enemy : MovingObject
{
    public int damage = 10;

    private Animator animator;
    private Transform target;
    private bool skipMove;
    private SpriteRenderer spriteRenderer;

    protected override void Start()
    {
        GameManager.instance.AddEnemyToList(this);

        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        target = GameObject.FindGameObjectWithTag("Player").transform;

        base.Start();
    }

    protected override bool AttemptMove<T>(int xDir, int yDir)
    {
        if (skipMove)
        {
            skipMove = false;
            return false;
        }

        bool move = base.AttemptMove<T>(xDir, yDir);

        skipMove = true;
        return move;
    }

    protected override void OnCantMove<T>(T component)
    {
        Player player = component as Player;
        player.LoseHealth(damage);

        animator.SetTrigger("enemyChop");
    }

    public void MoveEnemy()
    {
        int xDir = 0, yDir = 0;

        if (Mathf.Abs(target.position.x - transform.position.x) < float.Epsilon)
            yDir = (target.position.y > transform.position.y) ? 1 : -1;
        else
            xDir = (target.position.x > transform.position.x) ? 1 : -1;

        AttemptMove<Player>(xDir, yDir);
    }

    public SpriteRenderer GetSpriteRenderer()
    {
        return spriteRenderer;
    }
}